﻿[System.Serializable]
public class PlayerScoreData
{
    public int score;
    public string name;

    public static PlayerScoreData CreateData(string name, int score) {
        PlayerScoreData scoreData = new PlayerScoreData(name, score);
        return scoreData;
    }

    public PlayerScoreData(string name, int score)
    {
        this.score = score;
        this.name = name;
    }
}
