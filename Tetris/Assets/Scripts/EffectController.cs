﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectController : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> Effects;

    public void PlayEffect() {
        foreach (var item in Effects)
        {
            item.GetComponent<ParticleSystem>().Play();
        }
    }
}
