﻿using TMPro;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> Figures;
    private static int height = 25;
    private static int width = 10;
    public static Transform[,] grid = new Transform[width, height];
    public TextMeshProUGUI scoreText;
    public static int score = 0;
    private AudioController audioController;
    private EffectController effectController;

    private void Start()
    {
        audioController = FindObjectOfType<AudioController>();
        effectController = FindObjectOfType<EffectController>();
    }

    public void CreateFigure() {
        int index = Random.Range(0, Figures.Count);
        Instantiate(Figures[index], transform.position, Quaternion.identity);
    }

    public static void AddToGrid(Transform transform) {
        foreach (Transform children in transform)
        {
            int roundedX = Mathf.RoundToInt(children.transform.position.x);
            int roundedY = Mathf.RoundToInt(children.transform.position.y);
            if (roundedX < 10 || roundedY < 20)
            {
                grid[roundedX, roundedY] = children;
            }
        }
    }

    public void CheckForLines() {
        for (int i = 19; i >= 0; i--)
        {
            if (HasLine(i))
            {
                DeleteLine(i);
                audioController.Play("DeleteLine");
                effectController.PlayEffect();
                AddScore();
                RowDown(i);
            }
        }
    }

    private void AddScore()
    {
        score += 100;
        SetScore();
    }

    public void SetScore() {
        scoreText.text = $"You have {score} points";
    }

    private void RowDown(int i)
    {
        for (int y = i; y < 20; y++)
        {
            for (int j = 0; j < 10; j++)
            {
                if (grid[j, y] != null)
                {
                    grid[j, y - 1] = grid[j, y];
                    grid[j, y] = null;
                    grid[j, y - 1].transform.position -= new Vector3(0,1,0);
                }
            }
        }
    }

    private bool HasLine(int i)
    {
        for (int j = 0; j < 10; j++)
        {
            if (grid[j, i] == null)
            {
                return false;
            }
        }
        return true;
    }

    private void DeleteLine(int i)
    {
        for (int j = 0; j < 10; j++)
        {
            if (grid[j, i] != null)
            {
                Destroy(grid[j, i].gameObject);
                grid[j, i] = null;
            }
        }
    }

    public static bool CheckEndGame() {
        for (int i = 0; i < 10; i++)
        {
            if (grid[i, 19] != null)
            {
                return true;
            }
        }
        return false;
    }

    public void ClearGrid() {
        for (int i = 0; i < height; i++)
        {
            DeleteLine(i);
        }
      GameObject[] arr = GameObject.FindGameObjectsWithTag("Block");
        foreach (var item in arr)
        {
            Destroy(item);
        }
    }
}