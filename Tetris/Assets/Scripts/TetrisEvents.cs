﻿using System;
using UnityEngine;
#if UNITY_ANDROID
public class TetrisEvents : MonoBehaviour
{

    public static TetrisEvents current;

    public void Awake()
    {
        current = this;
    }

    public event Action onRotateButtonClick;
    public event Action onLeftButtonClick;
    public event Action onRightButtonClick;
    public event Action onFastFallButtonClick;
    public event Action onFastFallButtonRelease;

    public void RotateButtonClick() {
        if (onRotateButtonClick != null)
        {
            onRotateButtonClick();
        }
    }

    public void LeftButtonClick()
    {
        if (onLeftButtonClick != null)
        {
            onLeftButtonClick();
        }
    }

    public void RightButtonClick()
    {
        if (onRightButtonClick != null)
        {
            onRightButtonClick();
        }
    }

    public void FastFallButtonClick()
    {
        if (onFastFallButtonClick != null)
        {
            onFastFallButtonClick();
        }
    }

    public void FastFallButtonRelease()
    {
        if (onFastFallButtonRelease != null)
        {
            onFastFallButtonRelease();
        }
    }
}
#endif
