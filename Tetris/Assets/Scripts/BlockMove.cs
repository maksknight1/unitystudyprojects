﻿using UnityEngine;

#if UNITY_ANDROID
public class BlockMove : MonoBehaviour
{

    public void RotateBlock() {
        TetrisEvents.current.RotateButtonClick();
    }

    public void MoveBlockLeft()
    {
        TetrisEvents.current.LeftButtonClick();
    }

    public void MoveBlocRight()
    {
        TetrisEvents.current.RightButtonClick();
    }

    public void MoveBlockDownClick()
    {
        TetrisEvents.current.FastFallButtonClick();
    }

    public void MoveBlockDownRelease()
    {
        TetrisEvents.current.FastFallButtonRelease();
    }
}
#endif
