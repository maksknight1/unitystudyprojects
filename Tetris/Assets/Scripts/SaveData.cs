﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;
public static class SaveData
{
    private static string filePath = "/dataJ.Json";

    public static void JsonWrite(PlayerScoreData data) {
        string path = Application.persistentDataPath + filePath;
        // Read existing json data
        if (!File.Exists(path))
        {
            new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
        }
        var jsonData = File.ReadAllText(path);
        // De-serialize to object or create new list
        var ScoreList = JsonConvert.DeserializeObject<List<PlayerScoreData>>(jsonData)
                              ?? new List<PlayerScoreData>();
        // Add any new employees
        ScoreList.Add(data);
        // Update json data string
        jsonData = JsonConvert.SerializeObject(ScoreList);
        File.WriteAllText(path, jsonData);
        Debug.Log("Data is saved");
    }

    public static void JsonDelete() {
        string path = Application.persistentDataPath + filePath;
        if (File.Exists(path))
        {
            File.WriteAllText(path, string.Empty);
        }
    }

    public static List<PlayerScoreData> LoadJson() {
        List<PlayerScoreData> res = new List<PlayerScoreData>();
        string path = Application.persistentDataPath + filePath;
        string jsonData = "";
        if (File.Exists(path))
        {
            // Read existing json data
            jsonData = File.ReadAllText(path);
            if (jsonData == "")
            {
                res = null;
            }
            else
            {
                // De-serialize to object or create new list
                res = JsonConvert.DeserializeObject<List<PlayerScoreData>>(jsonData);
            }
        }
        return res;
    }

}
