﻿using UnityEngine;
using TMPro;

public class MenuController : MonoBehaviour
{
    [SerializeField]
    private GameObject PauseMenu;
    [SerializeField]
    private GameObject MainMenu;
    [SerializeField]
    private GameObject EndGameMenu;
    [SerializeField]
    private GameObject PauseBtn;
    [SerializeField]
    private GameObject SavePlayerDataMenu;
    [SerializeField]
    private TMP_InputField NameInput;
    [SerializeField]
    private RectTransform LeaderList;
    [SerializeField]
    private GameObject LeaderBoardMenu;
    [SerializeField]
    private RectTransform Content;
    [SerializeField]
    private GameObject AndroidButtons;
    private static bool GameIsPaused = false;
    private string PlayerName;
    private string PrevMenuName;
    
    public void StartGame() {
        PauseBtn.SetActive(true);
        MainMenu.SetActive(false);
#if UNITY_ANDROID
        AndroidButtons.SetActive(true);
#endif
        FindObjectOfType<GameController>().SetScore();
        FindObjectOfType<GameController>().CreateFigure();
    }

    public void UseMenu() {
        if (GameIsPaused)
        {
            Resume();
        }
        else
        {
            Pause();
        }
    }

    public void EndGame()
    {
        Time.timeScale = 0f;
#if UNITY_ANDROID
        AndroidButtons.SetActive(false);
#endif
        EndGameMenu.SetActive(true);
        PauseBtn.SetActive(false);
    }

    public void SavePlayerData() {
        TMP_Text SaveMessage = GameObject.Find("SaveMessage").GetComponent<TMP_Text>();
        SaveMessage.text = "";
        if (NameInput.text == "")
        {
            SaveMessage.text = "Player name is empty!";
            SaveMessage.color = Color.red;
        }
        else
        {
            if (NameInput.text.Length > 10)
            {
                SaveMessage.text = "Player name is to long!";
                SaveMessage.color = Color.red;
            }
            else
            {
                PlayerName = NameInput.text;
                PlayerScoreData data = PlayerScoreData.CreateData(PlayerName, GameController.score);
                string dataStr = $"{PlayerName}~{GameController.score};";
                SaveData.JsonWrite(data);
                SaveMessage.text = "Success!";
                SavePlayerDataMenu.SetActive(false);
                EndGameMenu.SetActive(true);
                SaveMessage.text = "";
            }
        }
        NameInput.text = "";
    }

    public void ShowSaveDataMenu() {
        EndGameMenu.SetActive(false);
        SavePlayerDataMenu.SetActive(true);
    }

    public void ShowScore()
    {
        LeaderBoardMenu.SetActive(true);
        PauseBtn.SetActive(false);
        CheckOpenMenu(PauseMenu);
        CheckOpenMenu(MainMenu);
        RenderLeaderList();
    }

    private void RenderLeaderList() {
        DeleteOldLeaders();
        if (SaveData.LoadJson() != null)
        {
            foreach (var item in SaveData.LoadJson())
            {
                CreateLeaderUI(LeaderList, Content, $"{item.name}-{item.score}");
            }
        }
        else
        {
            CreateLeaderUI(LeaderList, Content, $"No more Leaders");
        }
    }

    private static GameObject CreateLeaderUI(RectTransform LeaderList, RectTransform Content, string text) {
        var inst = Instantiate(LeaderList.gameObject) as GameObject;
        inst.transform.SetParent(Content, false);
        inst.GetComponent<RectTransform>().sizeDelta = new Vector2(250, 50);
        inst.GetComponent<TMP_Text>().text = text;
        return inst;
    }

    private void DeleteOldLeaders()
    {
        foreach (Transform child in Content)
        {
            Destroy(child.gameObject);
        }
    }

    public void ClearLeaders() {
        SaveData.JsonDelete();
        RenderLeaderList();
    }

    public void OpenPrevMenu() {
        LeaderBoardMenu.SetActive(false);
        CheckPrevMenu(PauseMenu);
        CheckPrevMenu(MainMenu);
    }

    public void Pause()
    {
        PauseMenu.SetActive(true);
#if UNITY_ANDROID
        AndroidButtons.SetActive(false);
#endif
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void Resume()
    {
        PauseMenu.SetActive(false);
#if UNITY_ANDROID
        AndroidButtons.SetActive(true);
#endif
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void NewGame()
    {
        CheckOpenMenu(EndGameMenu);
        Resume();
        PauseBtn.SetActive(true);
        FindObjectOfType<GameController>().ClearGrid();
        GameController.score = 0;
        FindObjectOfType<GameController>().SetScore();
        FindObjectOfType<GameController>().CreateFigure();
    }

    private void CheckOpenMenu(GameObject menu) {
        if (menu.activeSelf)
        {
            PrevMenuName = menu.name;
            menu.SetActive(false);
        }
    }

    private void CheckPrevMenu(GameObject menu) {
        if (menu.name == PrevMenuName)
        {
            menu.SetActive(true);
            if (menu.name == PauseMenu.name)
            {
                PauseBtn.SetActive(true);
            }
        }
    }

}
