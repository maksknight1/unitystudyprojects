﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TetrisBlockTest : MonoBehaviour
{
    public Vector3 retotionPoint;
    private float previousTime;
    public float fallTime = 1f;
    private int height = 25;
    private int width = 10;
    private bool isRotate = true;
    [SerializeField]
    private int minParX = 0;
    [SerializeField]
    private int minParY = 0;
    private MenuController menuController;
    private AudioController audioController;

    private void Start()
    {
#if UNITY_ANDROID
        TetrisEvents.current.onRotateButtonClick += Rotate;
        TetrisEvents.current.onLeftButtonClick += MoveLeft;
        TetrisEvents.current.onRightButtonClick += MoveRight;
        TetrisEvents.current.onFastFallButtonClick += FastFall;
        TetrisEvents.current.onFastFallButtonRelease += NormalFall;
#endif
        menuController = FindObjectOfType<MenuController>();
        audioController = FindObjectOfType<AudioController>();
    }

    private void Rotate()
    {
        transform.RotateAround(transform.TransformPoint(retotionPoint), new Vector3(0, 0, 1), 90);
        if (!ValidMove())
        {
            transform.RotateAround(transform.TransformPoint(retotionPoint), new Vector3(0, 0, 1), -90);
        }
        audioController.Play("BlocStep");
    }

    private void MoveLeft()
    {
        transform.position += new Vector3(-1, 0, 0);
        if (!ValidMove())
        {
            transform.position -= new Vector3(-1, 0, 0);
        }
        audioController.Play("BlocStep");
    }

    private void MoveRight()
    {
        transform.position += new Vector3(1, 0, 0);
        if (!ValidMove())
        {
            transform.position -= new Vector3(1, 0, 0);
        }
        audioController.Play("BlocStep");
    }

    public void FastFall() {
        fallTime = 0.1f;
    }

    public void NormalFall()
    {
        fallTime = 0.5f;
    }

    void Update()
    {
        Move();
        if (Time.time - previousTime > (Input.GetKey(KeyCode.DownArrow) ? fallTime /10 : fallTime))
        {
            transform.position += new Vector3(0, -1, 0);
            if (!ValidMove())
            {
                transform.position += new Vector3(0, 1, 0);
                GameController.AddToGrid(transform);
                FindObjectOfType<GameController>().CheckForLines();
                this.enabled = false;
#if UNITY_ANDROID
                TetrisEvents.current.onRotateButtonClick -= Rotate;
                TetrisEvents.current.onLeftButtonClick -= MoveLeft;
                TetrisEvents.current.onRightButtonClick -= MoveRight;
                TetrisEvents.current.onFastFallButtonClick -= FastFall;
                TetrisEvents.current.onFastFallButtonRelease -= NormalFall;
#endif
                if (!GameController.CheckEndGame())
                {
                    FindObjectOfType<GameController>().CreateFigure();
                }
                else
                {
                    menuController.EndGame();
                    audioController.Play("GameOver");
                }

            }
            audioController.Play("BlocStep");
            previousTime = Time.time;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Rotate();
        }
    }

    private void Move()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            MoveLeft();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            MoveRight();
        }
    }

    bool ValidMove() {
        foreach (Transform children in transform)
        {
            int roundedX = Mathf.RoundToInt(children.transform.position.x);
            int roundedY = Mathf.RoundToInt(children.transform.position.y);
           
            if (roundedY > 20)
            {
                return false;
            }
            if (roundedX < 0 || roundedX >= width || roundedY < 0 || roundedY >= height)
            {
                return false;
            }

            if (roundedY < 20)
            {
                if (GameController.grid[roundedX, roundedY] != null)
                {
                    return false;
                }
            }
        }
        return true;
    }
}
