﻿using UnityEngine;

/*
 * класс типа данных Sound
 */
[System.Serializable]
public class Sound 
{
    public string Name;
    public bool Effect;
    public AudioClip Clip;
    [Range(0f, 1f)]
    public float Volume;
    [Range(1f, 3f)]
    public float Pitch;
    [HideInInspector]
    public AudioSource Source;
    public bool Loop;
}
