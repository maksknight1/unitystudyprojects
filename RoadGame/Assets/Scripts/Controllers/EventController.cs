﻿using System;
using UnityEngine;

/*
 * класс для управления событиями в игре
 */
public class EventController : MonoBehaviour
{
    public static EventController current;
    public event Action OnMoveUp;
    public event Action OnMoveDown;
    public event Action OnMoveLeft;
    public event Action OnMoveRight;

    public void Awake()
    {
        current = this;
    }

    /*
    * метод в котором вызывается выполнения события OnMoveUp
    */
    public void MoveUpEvent()
    {
        OnMoveUp?.Invoke();
    }

    /*
    * метод в котором вызывается выполнения события OnMoveDown
    */
    public void MoveDownEvent()
    {
        OnMoveDown?.Invoke();
    }

    /*
    * метод в котором вызывается выполнения события OnMoveLeft
    */
    public void MoveLeftEvent()
    {
        OnMoveLeft?.Invoke();
    }

    /*
    * метод в котором вызывается выполнения события OnMoveRight
    */
    public void MoveRightEvent()
    {
        OnMoveRight?.Invoke();
    }
}
