﻿using UnityEngine;

/*
 * класс в котором происходит создание обьекта автомобиля на дороге
 */
public class Spawner : MonoBehaviour
{
    public GameObject car;
    public byte currentSpeed;
    float timerMax;

    // определение максимального времени таймера для создания нового обьекта машины, запуск метода для создания обьекта машины
    void Start()
    {
        timerMax = currentSpeed/(currentSpeed-1);
        InvokeRepeating("CreateRunCar", 1.0f, timerMax);
    }

    /* метод в котором происходит создание обьекта машины
     * GameObject item - параметр префаба
     */
    public void CreateRunCar() {
        car.GetComponent<CarController>().speed = currentSpeed;
        car.GetComponent<CarController>().move = transform.right;
        GameObject newCar = Instantiate(car, transform.position, Quaternion.identity);
        newCar.transform.parent = transform;
    }
}
