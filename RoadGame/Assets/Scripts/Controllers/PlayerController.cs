﻿using UnityEngine;

/*
 * класс для управлением поведения персонажа игрока
 */
public class PlayerController : MonoBehaviour
{
    public GameObject player;
    private Rigidbody rb;
    public static PlayerController thisPlayer;

    /*
     * на старте происходит привязка методов класса к событиям класса EventController
     */
    void Start()
    {
       // player = GameObject.FindGameObjectWithTag("Player");
        thisPlayer = this;
        rb = GetComponent<Rigidbody>();
        EventController.current.OnMoveUp += MoveUp;
        EventController.current.OnMoveDown += MoveDown;
        EventController.current.OnMoveLeft += MoveLeft;
        EventController.current.OnMoveRight += MoveRight;
    }

    /*
     * метод для определения состояния игры при соприкасновении с другим обьектом
     * Collision collision - данные о событии соприкосновения
     */
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Car")
        {
            GameController.current.LoseGame();
        }
        if (collision.gameObject.tag == "Win")
        {
            GameController.current.WinGame();
        }
    }

    /*
     * метод для перемещения вверх
     */
    public void MoveUp()
    {
        if (player.transform.position.z < 10)
        {
            rb.MovePosition(transform.position + transform.forward);
        }
    }

    /*
     * метод для перемещения вниз
     */
    public void MoveDown()
    {
        if (player.transform.position.z > 1)
        {
            rb.MovePosition(transform.position - transform.forward);
        }
    }

    /*
     * метод для перемещения влево
     */
    public void MoveLeft()
    {
        if (player.transform.position.x > -9)
        {
            rb.MovePosition(transform.position - transform.right);
        }
    }

    /*
     * метод для перемещения вправо
     */
    public void MoveRight()
    {
        if (player.transform.position.x < 9)
        {
            rb.MovePosition(transform.position + transform.right);
        }
    }
}
