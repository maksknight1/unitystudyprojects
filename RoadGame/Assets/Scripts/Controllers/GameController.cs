﻿using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

/*
 * класс в котором происходит управление поведения игры
 */
public class GameController : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> gameBlocs;
    private List<GameObject> gameField;
    public static GameController current;
    public int levelNum = 1;
    private Vector3 playerStartPos;
    private Random rndBloc = new Random();
    private Random rndSpeed = new Random();
    private Random rndAngles = new Random();

    // происходит иницыализация статического параметра класса для вызова в других классах
    void Start()
    {
        current = this;
        gameField = new List<GameObject>();
        StartGame(false);
    }

    /*
     * метод в котором происходит генерация игрового поля
     * bool type - параметр который определяет нужно ли уничтожать ранее созданное поле
     */
    public void StartGame(bool type) {
        if (type == true)
        {
            DestroyAllRoads();
        }
        for (byte i = 0; i < 10; i++)
        {
            if (i == 0)
            {
                CreateRoad("StartRoad", i, (byte)rndSpeed.Next(5, 15));
            }
            else if (i == 1 || i == 8)
            {
                CreateRoad("CarRoad", i, (byte)rndSpeed.Next(5, 15));
            }
            else if (i == 9)
            {
                CreateRoad("WinRoad", i, (byte)rndSpeed.Next(5, 15));
            }
            else {
                string nextBlock = gameBlocs[rndBloc.Next(1, 3)].name;
                if (gameField[i-1].name == "SafeRoad(Clone)")
                {
                    CreateRoad("CarRoad", i, (byte)rndSpeed.Next(5, 15));
                }
                else if(gameField[i - 1].name == "CarRoad(Clone)" & gameField[i - 2].name == "CarRoad(Clone)")
                {
                    CreateRoad("SafeRoad", i, (byte)rndSpeed.Next(5, 15));
                }
                else
                {
                    CreateRoad(nextBlock, i, (byte)rndSpeed.Next(5, 15));
                }
            }
        }
    }

    /*
     * метод в котором происходит убнуление положения игрока
     */
    public void ResetPlayerPosition() {
        PlayerController.thisPlayer.player.transform.position = playerStartPos;
    }

    /*
     * метод в котором происходит создание полосы локации
     * string name - параметр названия префаба для понимания что конкретно нужно строить
     * byte i - координата z на которой нужно расположить обьект
     * byte speed - скорость которая будет передана в класс Spawner
     */
    private void CreateRoad(string name, byte i, byte speed)  {
        GameObject fieldBlock;
        fieldBlock = Instantiate(gameBlocs.Find(item => item.name == name), transform.position, Quaternion.identity);
        if (name == "CarRoad")
        {
            fieldBlock.GetComponentInChildren<Spawner>().GetComponent<Spawner>().currentSpeed = speed;
            fieldBlock.transform.Rotate(00.0f, RotationType(), 0.0f, Space.Self);
        }
        if (name == "StartRoad")
        {
            playerStartPos = fieldBlock.GetComponentInChildren<PlayerController>().player.transform.position;
        }
        fieldBlock.transform.position = new Vector3(0, 0, i);
        fieldBlock.transform.parent = transform;
        gameField.Add(fieldBlock);
    }

    /*
     * Метод для определения угра поворота обьекта дороги
     */
    private float RotationType() {
        float angles = 0.0f;
        if (rndAngles.Next(0,2) >=1)
        {
            angles = 180.0f;
        }
        return angles;
    } 

    /*
     * метод в котором происходит уничтожение всех 
     */
    private void DestroyAllRoads() {
        foreach (GameObject road in gameField)
        {
            Destroy(road);
        }
        gameField.RemoveRange(0, gameField.Count);
    }

    /*
     * метод для продолжения игры в случае успешного прохождения текущего уровня
     */
    public void WinGame() {
        SoundController.current.Play("win");
        levelNum += 1;
        UIController.WinGame();
    }

    /*
     * метод в котором происходит вызов меню поражения
     */
    public void LoseGame() {
        SoundController.current.Play("scream");
        levelNum = 1;
        PauseCheck();
        UIController.DefeatGame();
    }

    /*
     * метод для проверки паузы в игре
     */
    public void PauseCheck() {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }
}
