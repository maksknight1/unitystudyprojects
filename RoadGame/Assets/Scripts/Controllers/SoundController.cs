﻿using System;
using UnityEngine;

/*
 * класс в котором происходит управление музыкой в игре
 */
public class SoundController : MonoBehaviour
{
    [SerializeField]
    private Sound[] sounds;
    public static SoundController current;

    public void Awake()
    {
        current = this;
        foreach (Sound s in sounds)
        {
            s.Source = gameObject.AddComponent<AudioSource>();
            s.Source.clip = s.Clip;
            s.Source.volume = s.Volume;
            s.Source.pitch = s.Pitch;
            s.Source.loop = s.Loop;
        }
        Play("loop");
    }

    /*
     * метод для проигрывания звуковой дорожки
     * принимает значение name
     */
    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.Name == name);
        s.Source.Play();
    }

    /*
    * метод в котором происходит управление громкости в игре через UI
    */
    public void ChangeVolume(bool type, float newVolume) {
        Sound[] soundsToChange = Array.FindAll(sounds, sound => sound.Effect == type);
        foreach (var s in soundsToChange)
        {
            s.Source.volume = newVolume;
        }
    }
}
