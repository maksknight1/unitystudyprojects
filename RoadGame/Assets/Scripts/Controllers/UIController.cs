﻿using UnityEngine;
using TMPro;

/*
 * класс в котором происходит управление интерфейсом
 */
public class UIController : MonoBehaviour
{
    private GameObject UI;
    private static GameObject pauseBtn;
    private GameObject pauseMenu;
    private GameObject mainMenu;
    private static GameObject swipeArea;
    private static GameObject defeatMenu;
    private static GameObject winMenu;
    private static GameObject currentLevel;
    private static GameObject optionMenu;
    private GameObject previousMenu;

    /*
     * на старте происходит определение каждого элемента интерфейса 
     */
    private void Start()
    {
        gameObject.transform.position = new Vector3(0,0,0);
        UI = GameObject.FindGameObjectWithTag("UI");
        pauseBtn = UI.transform.GetChild(2).gameObject;
        pauseMenu = UI.transform.GetChild(1).gameObject;
        mainMenu = UI.transform.GetChild(0).gameObject;
        defeatMenu = UI.transform.GetChild(3).gameObject;
        currentLevel = UI.transform.GetChild(4).gameObject;
        optionMenu = UI.transform.GetChild(5).gameObject;
        swipeArea = UI.transform.GetChild(6).gameObject;
        winMenu = UI.transform.GetChild(7).gameObject;
        previousMenu = mainMenu;
    }

    /*
     * проверка открыто ли меню настроек
     */
    public void ShowOptionMenu() {
        optionMenu.SetActive(true);
        previousMenu.SetActive(false);
    }

    /*
     * метод для открытия прошлой страницы меню
     */
    public void PrevMenu() {
        optionMenu.SetActive(false);
        previousMenu.SetActive(true);
    }

    /*
     * метод для управления громкости музыки 
     */
    public void Volume(float value) {
        SoundController.current.ChangeVolume(false, value);
    }

    /*
     * метод для управления звуковыми эффектами
     */
    public void Effects(float value) {
        SoundController.current.ChangeVolume(true, value);
    }

    /*
     * метод в котором происходит обновление показателя уровня
     */
    public static void ShowCurrentLevel(int num)
    {
        currentLevel.GetComponent<TMP_Text>().text = $"Current level: {num}";
    }

    /*
     * неактывные методы для управления через кнопки
     */
    //public void Up()
    //{
    //    PlayerController.thisPlayer.MoveUp();
    //}
    //public void Down()
    //{
    //    PlayerController.thisPlayer.MoveDown();
    //}

    //public void Left()
    //{
    //    PlayerController.thisPlayer.MoveLeft();
    //}

    //public void Right()
    //{
    //    PlayerController.thisPlayer.MoveRight();
    //}

    /*
     * метод в котором происходит вызов GameController.current.StartGame для создания игрового поля
     */
    public void StartGame()
    {
        currentLevel.SetActive(true);
        // GameController.current.StartGame(false);
        ShowCurrentLevel(1);
        defeatMenu.SetActive(false);
        pauseMenu.SetActive(false);
        mainMenu.SetActive(false);
        pauseBtn.SetActive(true);
        swipeArea.SetActive(true);
    }

    /*
     * метод в котором происходит вызов GameController.current.StartGame для создания игрового поля с уничтожением ранее созданного 
     */
    public void NewGame()
    {
        GameController.current.StartGame(true);
        ShowCurrentLevel(1);
        CheckActivePause();
        defeatMenu.SetActive(false);
        pauseMenu.SetActive(false);
        mainMenu.SetActive(false);
        pauseBtn.SetActive(true);
        swipeArea.SetActive(true);
    }

    /*
     * метод в котором происходит активация рекламы, с последующим обнулением позиции игрока на текущем игровом поле
     */
    public void RestartLevel() {
        AdsController.current.ShowVideoAdvertisements();
    }

    /*
     * метод в котором происходит подготовка к продолжению игры на текущем поле
     */
    public static void GameAfterAds() {
        defeatMenu.SetActive(false);
        pauseBtn.SetActive(true);
        swipeArea.SetActive(true);
        GameController.current.PauseCheck();
    }

    /*
     * метод для вызома меню проигрыша
     */
    public static void DefeatGame()
    {
        defeatMenu.SetActive(true);
        pauseBtn.SetActive(false);
        swipeArea.SetActive(false);
    }

    /*
    * метод для вызома меню победы
    */
    public static void WinGame() {
        winMenu.SetActive(true);
        pauseBtn.SetActive(false);
        swipeArea.SetActive(false);
    }

    /*
   * метод для активации нового уровня
   */
    public void StartNextLevel() {
        winMenu.SetActive(false);
        pauseBtn.SetActive(true);
        swipeArea.SetActive(true);
        ShowCurrentLevel(GameController.current.levelNum);
        GameController.current.StartGame(true);
    }

    /*
     * метод для закрытия приложения
     */
    public void ExitGame()
    {
        Application.Quit();
    }

    /*
     * метод для активации меню паузы
     */
    public void PauseGame() {
        CheckActivePause();
        pauseBtn.SetActive(false);
        swipeArea.SetActive(false);
    }

    /*
    * метод для проверки активности меню паузы
    */
    private void CheckActivePause()
    {
        GameController.current.PauseCheck();
        if (pauseMenu.activeSelf)
        {
            pauseMenu.SetActive(false);
        }
        else {
            pauseMenu.SetActive(true);
            previousMenu = pauseMenu;
        }
    }

    /*
     * метод для возвращения в игру
     */
    public void ResumeGame() {
        CheckActivePause();
        pauseBtn.SetActive(true);
        swipeArea.SetActive(true);
    }
}
