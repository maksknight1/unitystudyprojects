﻿using UnityEngine;

/*
 * класс в котором происходит управление поведением автомобиля
 */
public class CarController : MonoBehaviour
{
    private Rigidbody rb;
    public byte speed;
    public Vector3 move;
    [SerializeField]
    private GameObject car;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    /*
     * каждый фрейм смещение обьекта с заданной скоростью
     */
    void Update()
    {
      rb.MovePosition(transform.position + move * Time.deltaTime * speed);
    }

    /*
     * метод в котором происходит проверка собрикосновения автомобиля с точкой уничтожения
     */
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "CarDestroy")
        {
            Destroy(car);
        }
    }
}
