﻿using UnityEngine;
using UnityEngine.EventSystems;

/*
 * класс в котором происходит управление через свайп
 */
public class Swipe : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public static bool SwipeUp { get; private set; }
    public static bool SwipeDown { get; private set; }
    public static bool SwipeRight { get; private set; }
    public static bool SwipeLeft { get; private set; }
    private Vector2 startTouch;
    public Vector2 SwipeDelta { get; private set; }

    /*
     * метод для обнуления всех координат
     */
    private void Reset()
    {
        startTouch = SwipeDelta = Vector2.zero;
        SwipeUp = SwipeDown = SwipeRight = SwipeLeft = false;
    }

    /*
     * метод в котором происходит определение системы ввода
     */
    public void OnPointerDown(PointerEventData eventData)
    {
        #region Standalone
        if (Input.GetMouseButton(0))
        {
            startTouch = eventData.position;
            Debug.Log("startTouch - " + startTouch.ToString());
        }
        else if (Input.GetMouseButtonUp(0))
        {
            Reset();
        }
        #endregion
        #region Android
        if (Input.touches.Length > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                // startTouch = Input.touches[0].position;
                startTouch = eventData.position;
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                Reset();
            }
        }
        #endregion
    }

    /*
     * метод в котором происходит проверка направления движения и вызов метод для движения
     */
    private void CrossLine(Vector2 end)
    {
      if (Mathf.Abs(end.x - startTouch.x) > Mathf.Abs(end.y - startTouch.y))
      {
          // left or right
          if (end.x < startTouch.x)
          {
              SwipeLeft = true;
              EventController.current.MoveLeftEvent();
              Debug.Log("LEFT " + SwipeLeft);
          }
          else
          {
              SwipeRight = true;
              EventController.current.MoveRightEvent();
              Debug.Log("RIGHT " + SwipeRight);
          }
      }
      else
      {
          // up or down
          if (end.y < startTouch.y)
          {
              SwipeDown = true;
              EventController.current.MoveDownEvent();
              Debug.Log("DOWN " + SwipeDown);
          }
          else
          {
              SwipeUp = true;
              EventController.current.MoveUpEvent();
              Debug.Log("UP " + SwipeUp);
          }
      }
    }

    /*
     * проверка был ли отпущен тач или кнопка мыши
     */
    public void OnPointerUp(PointerEventData eventData)
    {
        Vector2 endTouch = eventData.position;
        Debug.Log("endTouch - " + endTouch.ToString() + "startTouch - " + startTouch.ToString());
        CrossLine(endTouch);
        Reset();
    }
}
