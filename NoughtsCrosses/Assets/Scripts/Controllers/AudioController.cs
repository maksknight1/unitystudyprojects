﻿using System;
using UnityEngine;

/*
 * класс в котором происходит управление музыкой в игре
 */
public class AudioController : MonoBehaviour
{

    public Sound[] Sounds;
    public static AudioController current;

    void Awake()
    {
        current = this;

        foreach (Sound s in Sounds)
        {
            s.Source = gameObject.AddComponent<AudioSource>();
            s.Source.clip = s.Clip;
            s.Source.volume = s.Volume;
            s.Source.pitch = s.Pitch;
            s.Source.loop = s.Loop; 
        }      
    }

    /*
     * метод для проигрывания звуковой дорожки
     * принимает значение name
     */
    public void Play(string name) {
        Sound s = Array.Find(Sounds, sound => sound.Name == name);
        s.Source.Play();
    }

}
