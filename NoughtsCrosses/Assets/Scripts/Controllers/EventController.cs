﻿using System;
using UnityEngine;

/*
 * класс в котором происходит управление созданными событиями
 */
public class EventController : MonoBehaviour
{
    public static EventController current;

    public void Awake()
    {
        current = this;
    }

    public event Func<PlayerData> OnFieldClick;

    /*
    * метод в котором вызывается выполнения события OnFieldClick
    */
    public PlayerData FieldClick() {
      return OnFieldClick?.Invoke();
    }
}
