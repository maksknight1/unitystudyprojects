﻿using System.Collections.Generic;
using UnityEngine;

/*
 * класс в котором происходит создание игроков и проверка хода
 */
public class GameController : MonoBehaviour
{

    private List<Sprite> spritesList;
    private static Player Player1;
    private static Player Player2;
    public static List<Player> Players = new List<Player>();
    private static Player CurrentPlayer;

    /*
     * происходит создание игроков и сбор картинок для них, установка какой игрок ходит первым
     * метод OnFieldClick связывается с методом SendPlayerData
     */
    void Start()
    {
        EventController.current.OnFieldClick += SendPlayerData;
        List<Sprite> spritesList = GetSprites();
        CurrentPlayer = Player1 = Player.CreatePlayer(0, spritesList[0], true, "Player1"); // игрок 1 ставит X
        Player2 = Player.CreatePlayer(1, spritesList[1], false, "Player2");
        Players.Add(Player1);
        Players.Add(Player2);
        AudioController.current.Play("main");

    }

    /*
     * метод в котором происходит подготовка игрового поля для новой игры
     */
    public static void StartPvPGame() {
        FieldController.ClearGameField();
        Player1.IsActive = true;
        Player2.IsActive = false;
        CurrentPlayer = Player1;
        UIController.ShowCurrentPlayer(CurrentPlayer.PlayerName);
    }

    /*
     * метод в котором происходит проверка какой игрок должен делать ход
     */
    public static void CheckPlayer()
    {
        foreach (var item in Players)
        {
            if (item.IsActive == false)
            {
                item.IsActive = true;
                CurrentPlayer = item;
            }
            else{item.IsActive = false;}
        }
        UIController.ShowCurrentPlayer(CurrentPlayer.PlayerName);
    }

    /*
     * метод в котором происходит отправка данных об игроке который совершил ход
     */
    public PlayerData SendPlayerData() {
        AudioController.current.Play("pick");
        PlayerData data = PlayerData.DataCreator(CurrentPlayer.TypeOfItem, CurrentPlayer.PlayerImage, CurrentPlayer.PlayerName);
        CheckPlayer();
        return data;
    }

    /*
     * метод в котором происходит сбор спрайтов для последующего распределения между игроками
     */
    private static List<Sprite> GetSprites() {
        List<Sprite> spritesList = new List<Sprite>();
        Object[] Sprites = Resources.LoadAll("Sprites", typeof(Sprite));
        foreach (var item in Sprites)
        { 
            spritesList.Add(item as Sprite);
        }
        return spritesList;
    }

}
