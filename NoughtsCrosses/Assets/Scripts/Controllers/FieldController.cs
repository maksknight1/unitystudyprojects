﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;

/*
 * класс в котором происходит управление состоянием игрового поля
 */
public class FieldController : MonoBehaviour
{
    private static FieldBlock[,] FieldBlocks { get; set; }
    private static FieldBlock[] prepeareArr { get; set; }

    /*
     * прогисходит сбор всех GameObject с тегом Field для формирования массива 3 на 3 в методе FillFild
     */
    void Start()
    {
        FieldBlocks = new FieldBlock[3, 3];
        prepeareArr = FindObjectsOfType<FieldBlock>();
        Array.Sort(prepeareArr, delegate (FieldBlock user1, FieldBlock user2) {
            return user1.Name.CompareTo(user2.Name); // (user1.Age - user2.Age)
        });
        FillFild(prepeareArr);
    }

    /*
    * метод в котором происходит заполнение массива FieldBlock[,] значениями типа FieldBlock для дальнейшего использования
    */
    private static void FillFild(FieldBlock[] prepeareArr) {
        byte count = 0;
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                FieldBlocks[i, j] = prepeareArr[count].GetComponent<FieldBlock>();
                count++;
            }
        }
    }

    /*
     * метод в котором происходит конечная проверка состояния поля на момент завершения хода игрока.
     * принимаемое значение передается в методы проверки сценариев победы и ничьи. 
     * возвращаемое значение значит:
     * 0 - победа одного из игроков
     * 1 - победа не была достигнута ни одним из игроков
     * 2 - ничья
     */
    public static byte CheckTurn(byte TypeOfItem) {
        if (ChecWinkLine(TypeOfItem) || CheckWinDiagonal(TypeOfItem))
        {
            AudioController.current.Play("win");
            return 0;
        }
        else if (CheckStandoff())
        {
            AudioController.current.Play("standof");
            return 2;
        }
        return 1;
    }

    /*
    * метод в котором происходит проверка была ли достигнута победа одного из игроков и передача данных для вывода
    * в случае победы игрока или ничьи происходит вызов метода ShowEndGameText и пеердача в него данных об игроке и результате проверки хода
    * в случае когда победа или ничья не были достигнуты происходит вызов ShowCurrentPlayer в который передается имя игрока который должен совершить код
    * метод принимает:
    * typeOfItem - тип итема игрока
    */
    public static void CheckWin(byte typeOfItem)
    {
        byte turnResult = CheckTurn(typeOfItem);
        if (turnResult != 1)
        {
            ChangeAll(false);
            UIController.ShowEndGameText(turnResult);
        }
    }

    /*
     * происходит проверка была ли достигнута победа по вертикали\горизонтали
     * принимает тип итема сделашего ход игрока для проверки достижения победы
     * возвращает true если победа была достигнута
     * возвращает false если победа не была достигнута
     */
    private static bool ChecWinkLine(byte item) {
        for (int i = 0; i < 3; i++)
        {
            if ((FieldBlocks[i, 0].typeOfItem == item && FieldBlocks[i, 1].typeOfItem == item && FieldBlocks[i, 2].typeOfItem == item) ||
                (FieldBlocks[0, i].typeOfItem == item && FieldBlocks[1, i].typeOfItem == item && FieldBlocks[2, i].typeOfItem == item))
            {
                return true;
            }
        }
        return false;
    }

    /*
     * происходит проверка была ли достигнута победа по диагоналям
     * принимает тип итема сделашего ход игрока для проверки достижения победы
     * возвращает true если победа была достигнута
     * возвращает false если победа не была достигнута
     */
    private static bool CheckWinDiagonal(byte item)
    {
        if ((FieldBlocks[0, 0].typeOfItem == item && FieldBlocks[1, 1].typeOfItem == item && FieldBlocks[2, 2].typeOfItem == item) ||
            (FieldBlocks[2, 0].typeOfItem == item && FieldBlocks[1,1].typeOfItem == item && FieldBlocks[0, 2].typeOfItem == item))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /*
     * проверка сценария ничьи
     * происходит когда на поле не осталось "пустых" клеток и победа не была достигнута
     */
    private static bool CheckStandoff() {
        bool res = true;
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                res = res & (FieldBlocks[i, j].typeOfItem != -10);
            }
        }
        if (res)
        {
            return true;
        }
        return false;
    }

    /*
    * метод в котором происходит блокирование всех пустых клеток в случае достижения победы
    */
    private static void ChangeAll(bool status) {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                FieldBlocks[i, j].GetComponent<EventTrigger>().enabled = status;
            }
        }
    }

    /*
    * метод в котором происходит очистка всех картинок которые загрузили пользователи
    */
    public static void ClearGameField() {
        ChangeAll(true);
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                FieldBlocks[i, j].ClearBlock();
            }
        }
    }

}
