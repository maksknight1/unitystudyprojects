﻿using UnityEngine;
using TMPro;

/*
 * класс в котором происходит управление интерфейсом игры
 */
public class UIController : MonoBehaviour
{
    private static TMP_Text currentPlayerText;
    private static TMP_Text endGameText;
    private GameObject mainMenu;
    private GameObject pauseMenu;
    private GameObject gameOverMenu;
    [SerializeField]
    private GameObject PauseBtn;
    public static UIController current;

    public void Awake()
    {
        current = this;
        GameObject[] menus = GameObject.FindGameObjectsWithTag("Menu");
        mainMenu = menus[0];
        pauseMenu = menus[1];
        gameOverMenu = menus[2];
        gameOverMenu.SetActive(false);
        pauseMenu.SetActive(false);
        PauseBtn.SetActive(false);
        GameObject[] texts = GameObject.FindGameObjectsWithTag("Text");
        currentPlayerText = texts[0].GetComponent<TMP_Text>();
        endGameText = texts[1].GetComponent<TMP_Text>();
    }

    /*
     * метод в котором происодит вывод имени игрока который должен совершить ход
     * принимаемое значение playerName
     */
    public static void ShowCurrentPlayer(string playerName) {
        currentPlayerText.text = $"Current player is: {playerName}";
    }

    /*
     * метод в котором пролисходит определение текста завершения игры
     * принимаемое значения - res
     * 1 - победа не была достигнута ни одним из игроков
     * 2 - ничья
     */
    public static void ShowEndGameText(byte res)
    {
        switch (res)
        {
            case 0:
                endGameText.text = $"Winner is: {GameController.Players.Find(p => p.IsActive == false).PlayerName}";
                currentPlayerText.text = $"";
                break;
            case 2:
                endGameText.text = $"Game Over - Standoff";
                currentPlayerText.text = $"";
                break;
            default:
                break;
        }
        current.EndGamePvP();
    }

    /*
    * метод для выхода из игры
    */
    public void Exit() {
        Application.Quit();
    }

    /*
    * метод для начала игры игрок против игрока
    */
    public void NewPvPGame()
    {
        mainMenu.SetActive(false);
        PauseBtn.SetActive(true);
        GameController.StartPvPGame();
        pauseMenu.SetActive(false);
        gameOverMenu.SetActive(false);
        endGameText.text = "";
    }

    /*
     * метод для активирования поузы
     */
    public void Pause()
    {
        CheckActivePause();
    }

    /*
     * метод для активации меню конца игры
     */
    private void EndGamePvP() {
        gameOverMenu.SetActive(true);
        PauseBtn.SetActive(false);
    }

    /*
     * метод для проверки активности меню паузы
     */
    private void CheckActivePause(){
        if (pauseMenu.activeSelf)
        {
            pauseMenu.SetActive(false);
        }
        else pauseMenu.SetActive(true);
    }

}
