﻿using UnityEngine;
using UnityEngine.UI;

/*
 * класс в котором происходит управление конкретной ячейкой
 */
public class FieldBlock : MonoBehaviour
{
    private bool isEmpty = true;
    public sbyte typeOfItem { get; private set; } = -10;
    public string Name { get ; set ; }

    public GameObject image;
    private PlayerData data;

    private void Awake()
    {
        Name = image.name;
    }

    /*
     * Метод в котором происходит сохранение картинки, типа значение 
     */
    public void SetImage()
    {
        if (isEmpty)
        {
            data = GameEvents.SendPlayerData();
            image.GetComponent<Image>().sprite = data.PlayerImage;
            typeOfItem = (sbyte)data.TypeOfPlayerItem;
            isEmpty = false;
            FieldController.CheckWin(data.TypeOfPlayerItem);
        }
    }

    /*
    * Метод в котором происходит обнуление данных в ячейках
    */
    public void ClearBlock() { 
        isEmpty = true;
        typeOfItem  = -10;
        image.GetComponent<Image>().sprite = null;
    }
}
