﻿using UnityEngine;

/*
 * класс для представления игрока
 */
public class Player
{
    public string PlayerName { get; private set; }
    public byte TypeOfItem { get; private set; }
    public Sprite PlayerImage { get; private set; }
    public bool IsActive { get;  set; }

    /*
     *  конструктор класса Player
     */
    public Player(byte typeOfItem, Sprite playerImage, bool isActive, string playerName)
    {
        TypeOfItem = typeOfItem;
        PlayerImage = playerImage;
        IsActive = isActive;
        PlayerName = playerName;
    }

    /*
     * статический метод для создания екземплярв класа игрока, принимает:
     * typeOfItem - тип итема игрока
     * playerImage - картинка которую использует игрок
     * isActive - статус активен ли игрок
     * playerName - имя игрока
     */
    public static Player CreatePlayer(byte typeOfItem, Sprite playerImage, bool isActive, string playerName) {
        return new Player(typeOfItem, playerImage, isActive, playerName);
    }

}
