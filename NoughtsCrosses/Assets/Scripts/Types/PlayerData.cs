﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * класс для представления передаваемых данных о том какой игрок совершил ход и информации о нем
 */
public class PlayerData
{
    public Sprite PlayerImage { get; private set; }
    public byte TypeOfPlayerItem { get; private set; }
    public string PlayerName { get; private set; }

    /*
     * констуктор PlayerData
     */
    public PlayerData(byte typeOfItem, Sprite playerImage, string playerName) {
        TypeOfPlayerItem = typeOfItem;
        PlayerImage = playerImage;
        PlayerName = playerName;
    }

    /*
     * статический метод для создания екземплярв класа игрока, принимает:
     * typeOfItem - тип итема игрока
     * playerImage - картинка которую использует игрок
     * playerName - имя игрока
     */
    public static PlayerData DataCreator(byte typeOfItem, Sprite playerImage, string playerName) {
        return new PlayerData(typeOfItem, playerImage, playerName);
    }
}
