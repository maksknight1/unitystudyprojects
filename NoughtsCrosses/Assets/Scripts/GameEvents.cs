﻿using UnityEngine;

/*
 * класс в котором содержатся методы для евентов в интерфейсе движка
 */
public class GameEvents : MonoBehaviour
{
    /*
    * метод который используется при клике по ячейкеполя для передачи данных сделавшего ход игрока 
    */
    public static PlayerData SendPlayerData() {
       return EventController.current.FieldClick();
    }
}
